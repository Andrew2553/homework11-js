const passwordInputs = document.querySelectorAll('input[type="password"]');
const showPasswordIcons = document.querySelectorAll('.icon-password');
const errorMessage = document.createElement('p');
errorMessage.style.color = 'red';
errorMessage.textContent = 'Потрібно ввести однакові значення';
errorMessage.style.display = 'none';

showPasswordIcons.forEach((icon, index) => {
  icon.addEventListener('click', () => {
    const passwordInput = passwordInputs[index];
    if (passwordInput.type === 'password') {
      passwordInput.type = 'text';
      icon.classList.remove('fa-eye');
      icon.classList.add('fa-eye-slash');
    } else {
      passwordInput.type = 'password';
      icon.classList.remove('fa-eye-slash');
      icon.classList.add('fa-eye');
    }
  });
});

const form = document.querySelector('.password-form');
form.appendChild(errorMessage);

form.addEventListener('submit', (event) => {
  event.preventDefault();
  const passwordInput1 = passwordInputs[0];
  const passwordInput2 = passwordInputs[1];
  if (passwordInput1.value === passwordInput2.value) {
    alert('You are welcome!');
  } else {
    errorMessage.style.display = 'block';
  }
});

passwordInputs.forEach((input) => {
  input.addEventListener('focus', () => {
    errorMessage.style.display = 'none';
  });
});
